import { TasksService, Task } from './../shared/tasks.service';
import { DateService } from './../shared/date.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import SwaggerUI from 'swagger-ui';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent implements OnInit {

  form: FormGroup;
  tasks: Task[] = [];

  constructor(public dateService: DateService,
              public tasksService: TasksService,
              private elRef: ElementRef) { }

  ngOnInit(): void {
    this.dateService.date.pipe(
      switchMap(value => this.tasksService.load(value))
    ).subscribe(tasks => {
      this.tasks = tasks;
    });

    this.form = new FormGroup({
      title: new FormControl('', Validators.required)
    });

    // this.createSwaggerUI();
  }

  submit() {
    const {title} = this.form.value;

    const task: Task = {
      title,
      date: this.dateService.date.value.format('DD-MM-YYYY')
    };

    this.tasksService.create(task)
    .subscribe(t => {
      this.tasks.push(t);
      this.form.reset();
    }, err => console.error(err));
  }

  createSwaggerUI() {
    const ui = SwaggerUI({
      url: 'https://petstore.swagger.io/v2/swagger.json',
      domNode: this.elRef.nativeElement.querySelector('.swagUIBlock'),
      deepLinking: true,
      presets: [
        SwaggerUI.presets.apis
      ],
    });
  }


  remove(task: Task) {
    this.tasksService.remove(task).subscribe(() => {
      this.tasks = this.tasks.filter(t => t.id !== task.id);
    }, err => console.error(err));
  }

}
